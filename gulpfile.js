var gulp = require('gulp'),
    gutil = require('gulp-util'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    prettify = require('gulp-html-prettify'),
    postcss = require('gulp-postcss'),
    flexbugs = require('postcss-flexbugs-fixes');



gutil.log('START!!!');

//
//Paths
//
var path = {
    build: {
        html:'./dist/',
        css: './dist/css/',
        img: './dist/img/'
    },
    src: {
        pug: './src/pug/*',
        scss: [
            './src/scss/**/*.scss'
        ],
        img: './src/img/**'
    },

    watch: {
        pug: './src/pug/*',
        scss: './src/scss/**'
    }
};


gulp.task('browserSync', function() {
    browserSync({
        server: {
            baseDir: "./dist/"
        },
        port: 8080,
        open: true,
        notify: false
    });
});


gulp.task('watcher',function(){
    gulp.watch(path.watch.pug, ['views']);
    gulp.watch(path.watch.scss, ['css_dev']);
});

//
//HTML
//


gulp.task('views', function () {
    return gulp.src(path.src.pug)
        .pipe(pug({
        }))
        .pipe(prettify({indent_char: ' ', indent_size: 2}))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}));
});


//
//CSS
//


gulp.task('css_dev', function () {
    return gulp.src(path.src.scss)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(postcss([flexbugs]))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream:true}));
});

gulp.task('css_prod', function () {
    return gulp.src(path.src.scss)
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(postcss([flexbugs]))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream:true}));
});



//
//IMAGES
//


gulp.task('images', function () {
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});



//
//General Tasks
//

gulp.task('dev',['views','css_dev', 'images']);

gulp.task('prod',['views', 'css_prod', 'images']);

gulp.task('default',['dev', 'watcher', 'browserSync']);

